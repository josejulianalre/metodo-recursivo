/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package método_recursivo;

/**
 *
 * @author Jose Julian Alvarez
 */
public class Método_Recursivo {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        int datos[] = new int[4];
        datos[0] = 12;
        datos[1] = 32;
        datos[2] = 4;
        datos[3] = 6;
        System.out.println("El numero mayor esta en la posición: " + PosicionMayor(datos));
    }

    public static int PosicionMayor(int[] datos) {
        if (datos.length == 1) {
            return 0;
        }
        return pMayor(datos, 0);
    }

    public static int pMayor(int[] datos, int n) {
        if (datos.length == n) {
            return -1;
        } else {

            int aux = pMayor(datos, n + 1), temp = -1;
            if (aux != -1) {
                temp = datos[aux];
            } else {
                return n;
            }
            if (datos[n] > temp) {
                return n;
            } else {
                return aux;
            }
        }
    }

}
